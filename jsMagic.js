//question9 and 10
let inputValue; //the user inputs var

//function to find if there are repeated numbers into an array
const find_duplicate_in_array = (inputCode) => {
    let object = {};
    let result = [];

    inputCode.forEach(function (item) {
        if(!object[item])
            object[item] = 0;
        object[item] += 1;
    })

    for (let prop in object) {
        if (object[prop] >= 3) {
            result.push(prop);
        }
    }
    return result;
}

//sum of all numbers of an array
const sum = (input) => {

    let total =  0;
    for (let i=0; i<input.length; i++) {
        if (isNaN(input[i])){
            continue;
        }
        total += Number(input[i]);
    }
    return total;
}

//function to handle all user inputs
const inputs = () => {
    inputValue = document.getElementById('userInput').value;

    let res = inputValue.split(''); //split it
    let duplicateResult = find_duplicate_in_array(res); //call the duplicate function and store the result into an array
    let successfulCount = 0;

    if (duplicateResult.length === 0) {
        successfulCount++;
    }

    let counterLength = 9;
    let multiply = new Array(9);
    for (let y=0; y<= res.length; y++) {
        if(counterLength <= 9 && counterLength >0) {
            multiply.push(res[y] * counterLength);
            counterLength --;
        }
    }

    let total = sum(multiply);
    if (total % 11 === 0) {
        successfulCount++;
    }

    //check the length of the string
    if (inputValue.length === 9) {
        successfulCount++;
    }

    if (successfulCount === 3) {
        const JSalertSuccess = () => { swal("Congrats!", ", Your code has been entered!", "success") }
        JSalertSuccess();
    }
    else {
        const JSalertFail = () => { swal("Try again", ", There is something wrong with the code you have entered!", "error") };
        JSalertFail();
    }
}




