<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="jsMagic.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <title>JS is fun</title>
</head>
<body>
    <div class="input-content">
        <form onsubmit="return false;">
            <div class="form-group">
               <h1>Promotion code</h1>
                <input type="number" class="form-control" id="userInput" placeholder="Enter the promotion code" >
            </div>
            <button onclick="inputs();" type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
</html>
